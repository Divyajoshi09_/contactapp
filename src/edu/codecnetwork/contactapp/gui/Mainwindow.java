package edu.codecnetwork.contactapp.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;public class Mainwindow {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private ContactDetails c;
	private JLabel lblNewLabel;

	/**
	 * Launch the application.
	 */
	//public static void main(String[] args) {
		//EventQueue.invokeLater(new Runnable() {
			//public void run() {
			//	try {
				//	Mainwindow window = new Mainwindow();
				//	window.frame.setVisible(true);
				//} catch (Exception e) {
				//	e.printStackTrace();
				//}
			//}
		//});
	//}

	/**
	 * Create the application.
	 */
	public Mainwindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, 1.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblEnterName = new JLabel("enter name");
		GridBagConstraints gbc_lblEnterName = new GridBagConstraints();
		gbc_lblEnterName.insets = new Insets(0, 0, 5, 5);
		gbc_lblEnterName.anchor = GridBagConstraints.EAST;
		gbc_lblEnterName.gridx = 0;
		gbc_lblEnterName.gridy = 0;
		panel.add(lblEnterName, gbc_lblEnterName);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 0;
		panel.add(textField, gbc_textField);
		textField.setColumns(10);
		
		JLabel lblEnterPhoneNo = new JLabel("enter phone no");
		GridBagConstraints gbc_lblEnterPhoneNo = new GridBagConstraints();
		gbc_lblEnterPhoneNo.anchor = GridBagConstraints.EAST;
		gbc_lblEnterPhoneNo.insets = new Insets(0, 0, 5, 5);
		gbc_lblEnterPhoneNo.gridx = 0;
		gbc_lblEnterPhoneNo.gridy = 1;
		panel.add(lblEnterPhoneNo, gbc_lblEnterPhoneNo);
		
		textField_1 = new JTextField();
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 5, 0);
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 1;
		panel.add(textField_1, gbc_textField_1);
		textField_1.setColumns(10);
		
		JLabel lblEnterEmail = new JLabel("enter email");
		GridBagConstraints gbc_lblEnterEmail = new GridBagConstraints();
		gbc_lblEnterEmail.anchor = GridBagConstraints.EAST;
		gbc_lblEnterEmail.insets = new Insets(0, 0, 5, 5);
		gbc_lblEnterEmail.gridx = 0;
		gbc_lblEnterEmail.gridy = 2;
		panel.add(lblEnterEmail, gbc_lblEnterEmail);
		
		textField_2 = new JTextField();
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.insets = new Insets(0, 0, 5, 0);
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.gridx = 1;
		gbc_textField_2.gridy = 2;
		panel.add(textField_2, gbc_textField_2);
		textField_2.setColumns(10);
		
		JButton btnSubmit = new JButton("submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c=new ContactDetails();
				c.setName(textField.getText().trim());
				c.setEmail(textField_1.getText());
				try {
					c.setPhoneNo(Long.parseLong(textField_2.getText().trim()));
				} catch (NumberFormatException e) {
					
					e.printStackTrace();
				} catch (Exception e) {
					label.setForeground(Color.RED);
					label.setText(e.getMessage());
					
					e.printStackTrace();
				}
			}
		});
		
		lblNewLabel = new JLabel("New label");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.gridheight = 3;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 3;
		panel.add(lblNewLabel, gbc_lblNewLabel);
		
	
		
		GridBagConstraints gbc_btnSubmit = new GridBagConstraints();
		gbc_btnSubmit.fill = GridBagConstraints.BOTH;
		gbc_btnSubmit.gridheight = 2;
		gbc_btnSubmit.gridx = 1;
		gbc_btnSubmit.gridy = 4;
		panel.add(btnSubmit, gbc_btnSubmit);
	}

}
