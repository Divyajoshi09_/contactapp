package edu.codecnetwork.contactapp.gui;

public class ContactDetails {
	private String name;
	private String email;
	private long PhoneNo;
	
	public String getName(){
		return name;
	}
	public void setName(String name)
	{
		this.name=name;
		
	}
	public String getEmail(){
		return email;
	}
	public void setEmail(String email)
	{
		this.email=email;
		
	}
	public long getPhoneNo(){
		return PhoneNo;
	}
	public void setPhoneNo(long PhoneNo) throws Exception
	{
		
	if (PhoneNo>19999999&&PhoneNo<9999999999l)
		this.PhoneNo=PhoneNo;
	else 
		throw new Exception("Phone No. out of Range");
	}
}